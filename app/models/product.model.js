const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    title:String,
    price:Number,
    quantity:Number,
    discription:String

},{
    timestamps:true
});

module.exports = mongoose.model('Product',ProductSchema);