module.exports = (app) => {
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
        next();
    });

    const products = require('../controllers/product.controller.js');



    //Create a new Product
    app.post('/products',products.create);


    //Retrieve all Products
    app.get('/products',products.findAll);

    //Retrive a single product with poductId
    app.get('/products/:productId',products.findOne)

    //Update a product with productid
    app.put('/products/:productId',products.update);


    //Delete a productId
    app.delete('/products/:productId',products.delete);

}