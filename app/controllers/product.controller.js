const Product = require('../models/product.model');
const multer = require('multer');
const upload = multer({dest:'/uploads/'});


//Create and Save a new Product
exports.create = (req,res) => {
    if(!req.body.discription){
        return res.status(400).send({
            message:"Product discription can't be empty"
        });
    }



    const product = new Product({
        title:req.body.title || "Untitled Product",
        price:req.body.price,
        quantity:req.body.quantity,
        discription:req.body.discription
    });

    product.save()
        .then(data =>{
            res.send(data);
        }).catch(err =>{
            res.status(500).send({
        message:err.message || "Some Error occured while createing the Product."
         });
    });


};

//Retrieve and return all products from database.
exports.findAll = (req,res) =>{
    Product.find()
        .then(products =>{
            res.send(products);
    }).catch(err =>{
        res.status(500).send({
        message:err.message || "Some error occured while retriving products."
    });
    });
};

//Find single product from Database
exports.findOne = (req,res) => {
Product.findById(req.params.productId)
    .then(product =>{
        if(!product){
            return res.status(404).send({
                message:"Product not found with Id"+req.params.productId
            });
    }
    res.send(product);
}).catch(err =>{
    if(err.kind === 'ObjectId'){
        return res.status(404).send({
            message:"Product Not found with id"+req.params.productId
        });
    }
    return res.status(500).send({
        message:"Error Retriving product with id"+req.params.productId
    });
    });
};

//Update a product indentified by the porductid in the Request
exports.update = (req,res) => {
    //Validate Request
    if(!req.body.discription){
        return res.status(400).send({
            message:"Product discription can't be empty"
        });
    }

    //Find product and update it with the request body
    Product.findByIdAndUpdate(req.params.productId,{
        title:req.body.title || "Untitled Product",
        price:req.body.price,
        quantity:req.body.quantity,
        discription:req.body.discription

    },{new:true})
        .then(product =>{
        if(!product){
        return res.status(404).send({
            message:"Product not found with id"+req.params.productId
        });
    }
    res.send(product);
    }).catch(err =>{
        if(err.kind === 'ObjectId'){
            return res.status(404).send({
                message:"Product not found with that id"+req.params.productId
            });
        }
        return res.status(500).send({
            message:"Error Updating product with id"+req.params.productId
        });
    });

};

//Delete a product with specified productid in the request
exports.delete = (req,res) => {
    Product.findByIdAndRemove(req.params.productId)
        .then(product =>{
            if(!product){
             return res.status(404).send({
                 message:"Product not found with that id"+req.params.productId
             });
            }
            res.send({message:"Product is Deleted successfully!"})
    }).catch(err =>{
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).send({
                message:"Product not found with that id"+req.params.productId
            });
        }
        return res.status(500).send({
            message:"Could not delete product with id"+req.params.productId
        });
    });

};