import {hashHistory} from 'react-router';
import initialState from './initialState';
import $ from 'jquery';
import toastr from 'toastr';


export default function applicationReducers(state = initialState, action) {

    switch (action.type) {

        case 'GO_TO_HOME_PAGE': {

            const newState = JSON.parse(JSON.stringify(state));
            newState.showForm = false;
            newState.selectedItem = {};
            newState.detailData = {};
            setTimeout(function () {
                hashHistory.push("/");
            }, 0);
            return newState;
        }
        case 'ON_SHOW_FORM': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.selectedItem = {};
            newState.detailData = {};
            newState.showForm = true;
            setTimeout(function () {
                hashHistory.push("ProductForm");
            }, 0);
            return newState;
        }
        case 'ON_HIDE_FORM': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.showForm = false;
            setTimeout(function () {
                hashHistory.push("/");
            }, 0);
            return newState;
        }
        case 'CREATE_PRODUCT': {
            const newState = JSON.parse(JSON.stringify(state));
            console.log("CREATE_PRODUCT",action.data)
            newState.showForm = false;
            setTimeout(function () {
                hashHistory.push("/");
            }, 0);
            return newState;
        }
        case 'UPDATE_PRODUCT': {
            const newState = JSON.parse(JSON.stringify(state));
            console.log("UPDATE_PRODUCT",action.data)
            newState.showForm = false;
            setTimeout(function () {
                hashHistory.push("/");
            }, 0);
            return newState;
        }
        case 'ON_EDIT_SELECTED_PRODUCT_CLICKED': {
            const newState = JSON.parse(JSON.stringify(state));
            console.log("ON_EDIT_SELECTED_PRODUCT_CLICKED",action.data)
            newState.selectedItem = action.data;
            newState.showForm = true;
            setTimeout(function () {
                hashHistory.push("ProductForm");
            }, 0);
            return newState;
        }

        case 'DELETE_SUCCESSFULLY': {
            const newState = JSON.parse(JSON.stringify(state));
            console.log("DELETE_SUCCESSFULLY",action.data)
            toastr.error(action.data.message);
            if(newState.detailView === true){
                setTimeout(function () {
                    hashHistory.push("/");
                }, 0);
                newState.detailView = false;
            }

            return newState;
        }
        case 'GET_ALL_PRODUCT_LIST': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.allProductInfo = action.data;
            console.log("newState.allProductInfo",newState.allProductInfo)

            return newState;
        }
        case 'ON_SHOW_DEAIL_PAGE': {
            const newState = JSON.parse(JSON.stringify(state));
            console.log("action.data",action.data);
            newState.detailData = action.data;
            newState.detailView = true;
            setTimeout(function () {
                hashHistory.push("DetailPage");
            }, 0);
            return newState;
        }
        default:
            return state;
    }
}