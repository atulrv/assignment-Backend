import productApi  from "../api/productApi";


export function gotoHomePage(data) {
    return {type: "GO_TO_HOME_PAGE", data};
}
export function onShowForm() {
    return {type: "ON_SHOW_FORM"};
}
export function onHideForm() {
    return {type: "ON_HIDE_FORM"};
}
export function onShowDeailPage(data) {
    return {type: "ON_SHOW_DEAIL_PAGE", data};
}


/* Get All Product start*/
export function onGetAllProductList() {
    return function (dispatch) {
        setTimeout(function () {
            productApi.getAllProduct().then(data => {
                return dispatch(getAllProductList(data));

            }).catch(error => {
                return dispatch(onError(error));
            });
        }, 0);
    };
}

export function getAllProductList(data) {
    return {type: "GET_ALL_PRODUCT_LIST", data};
}

/* Get All Product End */

export function onError(data) {
    return {type: "ON_ERROR", data};
}

/* save & update Product information start*/

export function saveProductsData(data) {
    return function (dispatch) {
        setTimeout(function () {
            productApi.addNewProduct(data).then(data => {
                return dispatch(createProduct(data));
            }).catch(error => {
                return dispatch(onError(error));
            });
        }, 0);
    };
}

export function createProduct(data) {
    return {type: "CREATE_PRODUCT", data};
}


export function updateProductsData(data) {
    return function (dispatch) {
        setTimeout(function () {
            productApi.updateProductItem(data).then(data => {
                return dispatch(updateProduct(data));
            }).catch(error => {
                return dispatch(onError(error));
            });
        }, 0);
    };
}



export function updateProduct(data) {
    return {type: "UPDATE_PRODUCT", data};
}
export function onEditSelectedProductClicked(data) {
    return {type: "ON_EDIT_SELECTED_PRODUCT_CLICKED", data};
}



/* save & update Product information End*/


/* Delete Start */

export function onClickedForDelete(data) {
    return function (dispatch) {
        setTimeout(function () {
            productApi.deleteProductItem(data).then(d => {
                return dispatch(deleteSuccessfully(d));
            }).catch(error => {
                return dispatch(onError(error));
            });
        }, 0);
    };
}

export function deleteSuccessfully(data) {
    return {type: "DELETE_SUCCESSFULLY", data};
}


/* Delete Ende */