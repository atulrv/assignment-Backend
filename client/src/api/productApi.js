import $ from 'jquery';
import config from './config';
import axios from 'axios';

class productApi {

/*
    static getAllProduct() {
        return (
            axios.get(config.baseURL + "products")
                .then(response => {
                    console.log("response",response)
                })
                .catch(error =>{
                    console.log("error",error)
                })

        )
    }
*/

    static getAllProduct() {
            return new Promise((resolve, reject) => {
                let request = $.ajax({
                    url: config.baseURL + "products",
                    method: "GET",
                    crossDomain:true,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                });
                request.done(function (responseData) {
                    console.log("responseData14",responseData)
                    resolve(responseData);
                });
                request.fail(function (jqXHR, textStatus) {
                    console.log("Fails")

                    if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                        console.log("23",jqXHR.responseJSON)

                        reject(jqXHR.responseJSON);
                    }
                    else if (jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                        console.log("28",jqXHR.responseText)

                        reject(jqXHR.responseText);
                    }
                    else {


                        reject({message: textStatus});
                        console.log("33",textStatus)
                    }
                });
            });
    }


    static addNewProduct(requestData) {
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                url: config.baseURL + "products",
                method: "POST",
                data: JSON.stringify(requestData.data),
                headers: {
                    "Content-Type": "application/json"
                }
            });
            request.done(function (responseData) {
                resolve(Object.assign({}, responseData));
            });
            request.fail(function (jqXHR, textStatus) {

                if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                    reject(jqXHR.responseJSON);
                }
                else if (jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                    reject(jqXHR.responseText);
                }
                else {
                    reject({message: textStatus});
                }
            });

        });
    }

    static updateProductItem(data){
        console.log("dataAPI",data.id)
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                url: config.baseURL + "products/" +data.id,
                method: "PUT",
                data: JSON.stringify(data.data),
                headers: {
                    "Content-Type": "application/json"
                }
            });
            request.done(function( responseData ) {
                resolve( Object.assign({}, responseData));
            });
            request.fail(function( jqXHR, textStatus ) {
                if(jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                    reject(jqXHR.responseJSON);
                }
                else if(jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                    reject(jqXHR.responseText);
                }
                else {
                    reject({message : textStatus});
                }
            });

        });
    }

    static deleteProductItem(data) {
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                url: config.baseURL + "products/" + data._id,
                method: "DELETE",
                data: JSON.stringify(data.data),
                headers: {
                    "Content-Type": "application/json"
                }
            });
            request.done(function (responseData) {
                resolve(Object.assign({}, responseData));
            });
            request.fail(function (jqXHR, textStatus) {

                if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                    reject(jqXHR.responseJSON);
                }
                else if (jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                    reject(jqXHR.responseText);
                }
                else {
                    reject({message: textStatus});
                }
            });

        });
    }


    static addBarCodeImage(requestData) {
        console.log("requestData",requestData)
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                url: config.baseURL + "Document/uploadFile",
                method: "POST",
                data: requestData,
                contentType: false,
                processData: false, // Don't process the files
            });
            request.done(function (responseData) {
                resolve(Object.assign({}, responseData));
            });
            request.fail(function (jqXHR, textStatus) {

                if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                    reject(jqXHR.responseJSON);
                }
                else if (jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                    reject(jqXHR.responseText);
                }
                else {
                    reject({message: textStatus});
                }
            });

        });
    }

    static addBarCodeId(requestData) {
        return new Promise((resolve, reject) => {
            let request = $.ajax({
                url: config.baseURL + "Document",
                method: "POST",
                data: JSON.stringify(requestData),
                headers: {
                    "Content-Type": "application/json"
                    //"Authorization": "Bearer" + " " + requestData.token
                }
            });
            request.done(function (responseData) {
                resolve(Object.assign({}, responseData));
            });
            request.fail(function (jqXHR, textStatus) {

                if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON !== null) {
                    reject(jqXHR.responseJSON);
                }
                else if (jqXHR.responseText !== undefined && jqXHR.responseText !== null) {
                    reject(jqXHR.responseText);
                }
                else {
                    reject({message: textStatus});
                }
            });

        });
    }




}


export default productApi;