import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/app';
import WelcomPage from './components/Home/welcome';
import DetailPage from './components/DetailView/DetailsViewPage';
import ProductForm from './components/Form/Form';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={WelcomPage}/>
        <path path="DetailPage" component={DetailPage} />
        <path path="ProductForm" component={ProductForm} />
    </Route>
);