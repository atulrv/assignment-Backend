import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';

class Header extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {

    }

    goToHome = () => {
        this.props.actionMaster.gotoHomePage()
    };

    ShowForm = () =>{
        this.props.actionMaster.onShowForm()
    };

    hideForm = () => {
        this.props.actionMaster.onHideForm()

    }
    render() {
        return (

                <nav className="navbar navbar-inverse navborderRadius">
                    <div className="container-fluid">
{/*
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">WebSiteName</a>
                        </div>
*/}
                        <ul className="nav navbar-nav">
                            <li><a className="glyphicon glyphicon-home" onClick={this.goToHome}></a></li>
                        </ul>
                        {
                            this.props.showForm === false ?
                                <ul className="nav navbar-nav navbar-right">
                                    <li onClick={this.ShowForm}><a href="#"><span className="glyphicon glyphicon-plus" ></span>Add Product</a></li>
                                    {/* <li><a href="#"><span className="glyphicon glyphicon-log-in"></span> Login</a></li>*/}
                                </ul>:
                                <ul className="nav navbar-nav navbar-right">
                                    <li onClick={this.hideForm}><a href="#"><span className="glyphicon glyphicon-remove" ></span>Back</a></li>
                                    {/* <li><a href="#"><span className="glyphicon glyphicon-log-in"></span> Login</a></li>*/}
                                </ul>


                        }
                    </div>
                </nav>



        );
    }

}

Header.propTypes = {
    actionMaster: PropTypes.object,
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
