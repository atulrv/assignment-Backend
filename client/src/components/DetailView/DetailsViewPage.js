import React, {PropTypes} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';
import Footer from "../common/Footer";


class DetailPage extends React.Component {
    constructor(props, context) {
        super(props, context);
    }


    onEdit = () => {
        this.props.actionMaster.onEditSelectedProductClicked(this.props.detailData)
    };

    onDelete = () => {
        this.props.actionMaster.onClickedForDelete(this.props.detailData)

    };


    render() {
        return (
            <div>
                <Header/>
                <div className="boxforData">

                    <div>
                        <img src={require('../../images/babu.jpg')} className="img-responsive" alt="Cinque Terre"/>
                    </div>

                    <hr/>

                    <div style={{textAlign: 'center'}}>
                        <ul style={{textAlign: 'center', listStyle: 'none'}}>
                            <li><span style={{fontSize: '16px'}}>Title: </span>{this.props.detailData.title}</li>
                            <li><span style={{fontSize: '16px'}}>Price: </span>{this.props.detailData.price}</li>
                            <li><span style={{fontSize: '16px'}}>Quantity: </span>{this.props.detailData.quantity}</li>
                            <li><span style={{fontSize: '16px'}}>Discription: </span>{this.props.detailData.discription}
                            </li>

                        </ul>

                    </div>
                    <div style={{marginTop: '80px'}}>
                        <span style={{paddingLeft: '190px'}} onClick={this.onEdit}>
                            <button type="button" className="btn btn-primary">Edit</button></span>
                        <span style={{paddingLeft: '290px'}} onClick={this.onDelete}>
                            <button type="button" className="btn btn-danger">Delete</button></span>

                    </div>
                </div>

                <Footer/>
            </div>


        );

    }


}


DetailPage.propTypes = {
    actionMaster: PropTypes.object,
    //allProductInfo: PropTypes.array
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
        //actionOrg: bindActionCreators(actionOrg, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPage);
