import React, {PropTypes} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';
import Footer from "../common/Footer";


class WelcomPage extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.actionMaster.onGetAllProductList();
    }

    getRandomColor(index) {
        var color = ['#f0433c', '#3f54b3', '#53ae54', '#fc9723', '#374046', '#2d98f1', '#992eae', '#fdc02b', '#1d9688', '#653fb5', '#feea4b', '#cedb46', '#785449', '#e80d8c', '#734396', '#488030'];
        return color[index];
    }

    getColorIndex = (index) => {
        return index % 15;
    };
    getCircleLabel = (text) => {
        if (text !== null && text !== undefined && text.length > 1) {
            return text[0].toUpperCase();
        }
    };

    onShowDetailView = (data) => {
        this.props.actionMaster.onShowDeailPage(data)
    };

    onEdit = (data) => {
        console.log("data37",data)
        this.props.actionMaster.onEditSelectedProductClicked(data)
    };

    onDelete= (data) => {
        this.props.actionMaster.onClickedForDelete(data)

        setTimeout(function () {
            this.props.actionMaster.onGetAllProductList();

        }.bind(this),500)
    };

    render() {


        return (
            <div>
                <Header/>
                {
                    this.props.allProductInfo.length !== 0 ?
                    this.props.allProductInfo.map((data, i) => {
                        return <li className="list-group-item">
                             <span className="badgeLcircle"
                                   style={{
                                       cursor: 'pointer',
                                       backgroundColor: this.getRandomColor(this.getColorIndex(i))
                                   }}
                                   onClick={() => this.onShowDetailView(data)}>{this.getCircleLabel(data.title)}</span>
                            <label style={{paddingLeft: '20px'}}>{data.title}</label>

                            <button className="badgeL pull-right marforButton" style={{
                                color: this.getRandomColor(this.getColorIndex(i)),
                                fontSize: '20px'
                            }} onClick={() => this.onEdit(data)}><span
                                className="glyphicon glyphicon-pencil"></span></button>

                            <button className="badgeL mr15 marforButton pull-right" style={{
                                color: this.getRandomColor(this.getColorIndex(i)),
                                fontSize: '20px'
                            }} onClick={() => this.onDelete(data)}><span
                                className="glyphicon glyphicon-remove"></span></button>


                        </li>

                    }):
                        <div className="nodataMessage"><span className="glyphicon glyphicon-info-sign"
                                                             aria-hidden="true"></span> No Data</div>
                }

                <Footer/>
            </div>


        );

    }


}


WelcomPage.propTypes = {
    actionMaster: PropTypes.object,
    allProductInfo: PropTypes.array
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
        //actionOrg: bindActionCreators(actionOrg, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomPage);
