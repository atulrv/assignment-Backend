"use strict";

module.exports = {
    options: [
        "setClasses"
    ],
    "feature-detects": [
        "css/flexbox",
        "es6/promises",
        "serviceworker",
        "svg"
    ]
};