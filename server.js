const express = require('express');
const bodyParser = require('body-parser');
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

//create express app

const app = express();

//parse request of content-type-application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}));

//parse request of content-type - application/json
app.use(bodyParser.json())

//Cofiguring the database

mongoose.connect(dbConfig.url)
    .then(() =>{
        console.log("Successfully connected to database");
}).catch(err =>{
    console.log('Could not connect to the database.Exiting now....');
    process.exit();
})

//define a simple route
app.get('/',(req,res) => {
    res.json({"message":"welcome to application"});
});

//Require Product Routes
require('./app/routes/product.routes')(app);
//listen for requests
app.listen(5000,() =>{
    console.log("Server is listening on port 5000");
});