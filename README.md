# assignment-Backend

To be able to run/build the web application, Node.js (https://nodejs.org/en/) must be installed.
***
Before running/building the project, run the command _**'npm install'**_ then go for client
 cd clint run the command _**'npm install'**
 ***
 **To** then start your mongodDb.

***
**To** run the application in development mode use command _**'npm start'**_
***
**To** change the rest api endpoint modify endpoint in file `'src/api/config.js'`
***
**To** Build the distribution use command _**'npm run build' for client**_
***

**To** run application, open dist/index.html into browser or host dist folder on web server and access dist/index.html. 
***
**_``****__``_**